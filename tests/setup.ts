import Vue from 'vue'
import Vuetify from 'vuetify'

export const vuetify = new Vuetify()
Vue.use(Vuetify)
