import { shallowMount } from '@vue/test-utils'
import { vuetify } from '../setup'
import Home from '@/views/Home.vue'

describe('Home.vue', () => {
  it('is called "home"', () => {
    const wrapper = shallowMount(Home, {
      vuetify
    })
    expect(wrapper.name()).toEqual('home')
  })
})
