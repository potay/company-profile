FROM node:alpine AS builder

WORKDIR /opt/comprof
RUN mkdir -p /opt/comprof

COPY . .
RUN npm -g install yarn
RUN yarn install --frozen-lockfile
RUN yarn build
RUN rm dist/report.html


FROM nginx:alpine
ENV ACCESS_LOG=/var/log/comprof/access_main.log
ENV ERROR_LOG=/var/log/comprof/error_main.log
VOLUME ["/var/log/comprof"]

COPY --from=builder /opt/comprof/dist/ /usr/share/nginx/html/
COPY --from=builder /opt/comprof/nginx.conf /etc/nginx/conf.d/default.conf.template
COPY --from=builder /opt/comprof/start.sh /opt/comprof/start.sh

RUN chmod +x /opt/comprof/start.sh
EXPOSE 80 443
ENTRYPOINT ["/opt/comprof/start.sh"]
