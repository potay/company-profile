#!/bin/sh
set -eu
envsubst '${ACCESS_LOG} ${ERROR_LOG} ${WEBSERVICE_URL} ${RASPI_SAMPLE_URL} ${PREDICTOR_URL}' < /etc/nginx/conf.d/default.conf.template > /etc/nginx/conf.d/default.conf
nginx -g "daemon off;"
