// Cache the Google Fonts stylesheets with a stale while revalidate strategy.
workbox.routing.registerRoute(
  /^https:\/\/fonts\.googleapis\.com/,
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'google-fonts-stylesheets',
  }),
);

// Cache the Google Fonts webfont files with a cache first strategy for 1 year.
workbox.routing.registerRoute(
  /^https:\/\/fonts\.gstatic\.com/,
  new workbox.strategies.CacheFirst({
    cacheName: 'google-fonts-webfonts',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200],
      }),
      new workbox.expiration.Plugin({
        maxAgeSeconds: 60 * 60 * 24 * 365,
      }),
    ],
  }),
);

// Cache JS and CSS with background revalidate
workbox.routing.registerRoute(
  /\.(?:js|css)$/,
  new workbox.strategies.StaleWhileRevalidate(),
);

// Cache images
workbox.routing.registerRoute(
  /\.(?:png|gif|jpg|jpeg|svg)$/,
  new workbox.strategies.CacheFirst({
    cacheName: 'images',
    plugins: [
      new workbox.expiration.Plugin({
        maxEntries: 100,
        maxAgeSeconds: 365 * 24 * 60 * 60, // 1 Year
      }),
    ],
  }),
);

// Skip waiting
workbox.core.skipWaiting();

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

// Clients claim
workbox.core.clientsClaim();

// Cleanup outdated caches
workbox.precaching.cleanupOutdatedCaches();

// Offline page
workbox.routing.registerNavigationRoute(
  workbox.precaching.getCacheKeyForURL('/index.html'), {
    blacklist: [
      new RegExp('/api/'),
      new RegExp('/predictor/'),
      new RegExp('/sample/'),
    ],
  }
);

// Cache requests
const bgSyncPlugin = new workbox.backgroundSync.Plugin('apiqueue', { maxRetentionTime: 24 * 60 });
workbox.routing.registerRoute(/api/, new workbox.strategies.NetworkFirst(), 'GET');
workbox.routing.registerRoute(/api\/history/, new workbox.strategies.NetworkOnly({ plugins: [bgSyncPlugin] }), 'DELETE');
workbox.routing.registerRoute(/api\/csrf/, new workbox.strategies.NetworkFirst(), 'GET');
workbox.routing.registerRoute(/api\/account\/refresh-token/, new workbox.strategies.NetworkOnly({ plugins: [bgSyncPlugin] }), 'POST');
workbox.routing.registerRoute(/api\/account\/verify-token/, new workbox.strategies.NetworkOnly({ plugins: [bgSyncPlugin] }), 'POST');
workbox.routing.registerRoute(/predictor\/csrf/, new workbox.strategies.NetworkFirst(), 'GET');
workbox.routing.registerRoute(/predictor\/explain/, new workbox.strategies.NetworkFirst(), 'POST');
workbox.routing.registerRoute(/sample/, new workbox.strategies.NetworkFirst(), 'GET');
workbox.routing.registerRoute(/sample/, new workbox.strategies.NetworkOnly({ plugins: [bgSyncPlugin] }), 'POST');
