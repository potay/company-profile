export default {
  getEmotionFromData (color: string, texture: string): string {
    if (texture.toLowerCase() === 'normal') {
      if (['brown', 'green'].indexOf(color.toLowerCase()) >= 0) {
        return 'happy'
      }
    }
    return 'sad'
  },

  getFaceUrl (color: string, confidence: boolean, texture: string): string {
    const emotionText: string = this.getEmotionFromData(color, texture)
    const confidenceText: string = confidence ? 'certain' : 'uncertain'
    texture = texture.toLowerCase()
    return `face-${texture}-${emotionText}-${confidenceText}.png`
  },

  getBodyUrl (color: string, texture: string): string {
    color = color.toLowerCase()
    texture = texture.toLowerCase()
    return `${texture}-${color}.png`
  }
}
