const WEEKDAY_NAMES = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']
const MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June',
  'July', 'August', 'September', 'October', 'November', 'December']

const leftPad = function (string: any, pad: number, padCharacter: string = ' ') {
  string = string.toString()
  const padLength = pad - string.length
  if (padLength > 0) {
    return new Array(padLength + 1).join(padCharacter) + string
  } else {
    return string
  }
}

export const dateName = {
  prettifyDateTime: function (dateString: string) {
    const date = new Date(dateString)
    if (isNaN(date.getTime())) {
      return ''
    }
    const textDay = WEEKDAY_NAMES[date.getDay()]
    const textDayInt = date.getDate()
    const textMonth = MONTH_NAMES[date.getMonth()]
    const textYear = date.getFullYear()

    const timeString = date.toTimeString()
    const timeArray = timeString.split(':')
    let timeHour = parseInt(timeArray[0])
    const timeMin = parseInt(timeArray[1])
    return `${textDay}, ${textMonth} ${textDayInt} ${textYear} - ${leftPad(timeHour, 2)}:${leftPad(timeMin, 2, '0')}`
  }
}
