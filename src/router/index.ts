import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: () => import('@/views/Home.vue')
  },
  {
    path: '/sign-in',
    name: 'signin',
    component: () => import('@/views/SignIn.vue')
  },
  {
    path: '/sign-up',
    name: 'signup',
    component: () => import('@/views/SignUp.vue')
  },
  {
    path: '/start',
    name: 'start',
    component: () => import('@/views/Start.vue')
  },
  {
    path: '/questions',
    name: 'questionnaire',
    component: () => import('@/views/Questionnaire.vue')
  },
  {
    path: '/process',
    name: 'resultprocessing',
    component: () => import('@/views/ResultProcessing.vue')
  },
  {
    path: '/result',
    name: 'result',
    component: () => import('@/views/Result.vue')
  },
  {
    path: '/history/:id',
    name: 'logdetail',
    component: () => import('@/views/LogDetail.vue')
  },
  {
    path: '/history',
    name: 'history',
    component: () => import('@/views/History.vue')
  },
  {
    path: '/request-reset-password',
    name: 'requestresetpassword',
    component: () => import('@/views/RequestResetPassword.vue')
  },
  {
    path: '/reset-password',
    name: 'resetpassword',
    component: () => import('@/views/ResetPassword.vue')
  },
  {
    path: '/decode',
    name: 'decode',
    component: () => import('@/views/Decode.vue')
  },
  {
    path: '/error',
    name: 'notfound-404',
    component: () => import('@/views/NotFound.vue')
  },
  {
    path: '*',
    name: 'notfound',
    component: () => import('@/views/NotFound.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }

    if (savedPosition) {
      return savedPosition
    }

    return { x: 0, y: 0 }
  },
  base: process.env.BASE_URL,
  routes
})

export default router
