import Vue from 'vue'
import App from '@/App.vue'
import '@/registerServiceWorker'
import VueCookies from 'vue-cookies'
import router from '@/router'
import store from '@/store'
import vuetify from '@/plugins/vuetify'

Vue.config.productionTip = false

Vue.use(VueCookies)
Vue.cookies.config('7d')

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
