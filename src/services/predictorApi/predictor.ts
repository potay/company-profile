import api from '@/services/predictorApi'

export default {
  predict (image: Buffer, quizJson: number[]) {
    const imageFile = new File([image], 'encoded.bin')
    const data = new FormData()
    data.append('image', imageFile)
    data.append('quiz_json', JSON.stringify(quizJson))
    return api().post('predict/', data)
  },
  testDecode (image: Buffer) {
    const imageFile = new File([image], 'encoded.bin')
    const data = new FormData()
    data.append('image', imageFile)
    return api().post('test-decode/', data, { responseType: 'blob' })
  },
  getExplanations (confident: boolean, texture: string, color: string, quizJson: number[]) {
    return api().post('explain/', {
      'confident': confident,
      'texture': texture,
      'color': color,
      'quiz_json': JSON.stringify(quizJson)
    })
  }
}
