import axios, { AxiosInstance } from 'axios'
import Cookies from 'js-cookie'

export default (): AxiosInstance => {
  let csrfToken: string | undefined = Cookies.get('csrftoken')
  let baseUrl: string = '/predictor'
  if (csrfToken === null || csrfToken === undefined) {
    axios.create({
      baseURL: baseUrl,
      timeout: 60000
    }).get('csrf/').then(response => {
      Cookies.set('csrftoken', response.data.result)
    })
  }
  return axios.create({
    baseURL: baseUrl,
    timeout: 60000,
    headers: {
      'Content-Type': 'application/json',
      'X-CSRFToken': Cookies.get('csrftoken')
    }
  })
}
