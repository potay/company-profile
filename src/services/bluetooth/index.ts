import { BluetoothVuexDeviceOption } from '@/store/webBluetooth'

export class BluetoothDeviceOption {
  anyDevices: boolean;
  name?: string;
  namePrefix?: string;
  services: BluetoothServiceOption[];

  constructor (services: BluetoothServiceOption[], name?: string, namePrefix?: string) {
    this.services = services
    this.anyDevices = false
    if (name) {
      this.name = name
    } else if (namePrefix) {
      this.namePrefix = namePrefix
    } else {
      this.anyDevices = true
    }
  }

  toVuexDeviceOption (): BluetoothVuexDeviceOption {
    let filterServices: string[] = []
    for (let service of this.services) {
      filterServices.push(service.uuid)
    }
    return {
      anyDevices: this.anyDevices,
      name: this.name,
      namePrefix: this.namePrefix,
      services: filterServices
    }
  }
}

export interface BluetoothServiceOption {
  name: string;
  uuid: string;
  [characteristicsName: string]: string;
}

export class BluetoothIOProgress {
  totalLength: number = 0;
  currentLength: number = 0;

  getPercentage (): number {
    const originalPercentage: number = (this.currentLength / this.totalLength) * 100
    if (originalPercentage > 100) {
      return 100
    }
    return originalPercentage
  }
}

export async function readLargeValue (characteristic: BluetoothRemoteGATTCharacteristic, progressObj: BluetoothIOProgress) {
  const initPattern: RegExp = /^##\d{1,18}##$/g
  let result: Buffer = Buffer.from('')
  let chunk: Buffer = Buffer.from('')
  let chunkLengths: number[] = []
  progressObj.totalLength = 15 * 1024 // 15 KB is the average payload size, we use it as init assumption.
  progressObj.currentLength = 0

  let time: number = Date.now() / 1000
  do {
    chunk = await characteristic.readValue().then(value => {
      return Buffer.from(value.buffer)
    })
    if (chunkLengths.length === 0 && initPattern.test(chunk.toString())) {
      progressObj.totalLength = parseInt(chunk.toString().replace('##', ''))
    } else {
      result = Buffer.concat([result, chunk])
      progressObj.currentLength += chunk.length
    }
    chunkLengths.push(chunk.length)
  } while (chunk.toString() !== '')
  let spent: number = (Date.now() / 1000) - time
  console.log('Chunks length: ' + chunkLengths.toString())
  console.log('Result length: ' + result.length.toString())
  console.log('Time spent: ' + spent.toString())
  console.log('Speed (B/s): ' + (result.length / spent).toString())
  return result
}

export async function writeLargeValue (characteristic: BluetoothRemoteGATTCharacteristic, value: string, progressObj: BluetoothIOProgress) {
  let toWrite: string
  let toWriteBytes: Uint8Array | null
  progressObj.totalLength = value.length
  progressObj.currentLength = 0

  while (value.length > 0) {
    toWrite = value.substring(0, 512) // use first 512 bytes
    value = value.substring(512) // remove first 512 bytes
    toWriteBytes = new TextEncoder().encode(toWrite)
    await characteristic.writeValue(toWriteBytes)
    progressObj.currentLength += 512
  }

  // Send empty frame to sign end of transmission
  toWriteBytes = new TextEncoder().encode('')
  await characteristic.writeValue(toWriteBytes)
}
