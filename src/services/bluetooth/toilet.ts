import { BluetoothDeviceOption } from '@/services/bluetooth'

export default class Toilet extends BluetoothDeviceOption {
  constructor () {
    super(
      [
        {
          name: 'main',
          uuid: 'a42242ed-a839-42da-a89d-9bc41cbb8c6d',
          captureUuid: '1ec7fc95-3a0d-4f8e-b436-f6d5f10d316a'
        }
      ]
    )
  }
}
