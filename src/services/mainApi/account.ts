import api from '@/services/mainApi'

export default {
  createAccount (email: string, username: string, password: string) {
    return api().post('account/create/', {
      'email': email,
      'username': username,
      'password': password
    })
  },
  forgotPassword (email: string) {
    return api().post('account/password/forgot/', { 'email': email })
  },
  changePassword (password: string, confirmPassword: string) {
    return api().post('account/password/change/', {
      'password': password,
      'confirm_password': confirmPassword
    })
  },
  changePasswordLink (token: string, password: string, confirmPassword: string) {
    return api().post('account/password/' + token + '/', {
      'password': password,
      'confirm_password': confirmPassword
    })
  },
  changePasswordLinkCheck (token: string) {
    return api().head('account/password/' + token + '/')
  },
  deleteAccount () {
    return api().delete('account/delete/')
  },
  login (username: string, password: string) {
    return api().post('account/login/', {
      'username': username,
      'password': password
    })
  },
  refreshToken (refreshToken: string) {
    return api().post('account/refresh-token/', { 'refresh': refreshToken })
  },
  verifyToken (accessToken: string) {
    return api().post('account/verify-token/', { 'token': accessToken })
  }
}
