import api from '@/services/mainApi'

export default {
  fetchHistory (page: number = 0, perPage: number = 25) {
    return api().get('history/', {
      data: { 'page': page, 'per_page': perPage }
    })
  },
  fetchHistoryDetail (id: number = 0) {
    return api().get('history/?id=' + id)
  },
  clearHistory () {
    return api().delete('history/')
  }
}
