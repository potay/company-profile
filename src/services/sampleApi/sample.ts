import api from '@/services/sampleApi'

export default {
  demo () {
    return api().get('demo/', { responseType: 'blob' })
  }
}
