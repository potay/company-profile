import axios, { AxiosInstance } from 'axios'

export default (): AxiosInstance => {
  return axios.create({
    baseURL: '/sample',
    timeout: 60000,
    headers: {
      'Content-Type': 'application/json'
    }
  })
}
