import Vue from 'vue'
import Axios from 'axios'
import { ActionTree, GetterTree, MutationTree } from 'vuex'
import accountApi from '@/services/mainApi/account'
import { RootState } from '@/store'

interface LoginPayload {
  username: string;
  password: string;
}

export interface AuthState {
  jwtAccess: string;
  jwtRefresh: string;
}

const state: AuthState = {
  jwtAccess: null,
  jwtRefresh: null
}

const getters: GetterTree<AuthState, RootState> = {
  jwtAccess: (state: AuthState) => {
    return state.jwtAccess
  },
  jwtRefresh: (state: AuthState) => {
    return state.jwtRefresh
  }
}

const actions: ActionTree<AuthState, RootState> = {
  loadCookies ({ commit }) {
    var accessToken: string | null = Vue.cookies.get('access_token') || null
    var refreshToken: string | null = Vue.cookies.get('refresh_token') || null
    if (accessToken !== '' && accessToken !== null) {
      commit('setJwtAccess', accessToken)
    } else {
      commit('removeJwtAccess')
    }
    if (refreshToken !== '' && refreshToken !== null) {
      commit('setJwtRefresh', refreshToken)
    } else {
      commit('removeJwtRefresh')
    }
  },
  async login ({ commit }, payload: LoginPayload) {
    return accountApi.login(payload.username, payload.password).then((response) => {
      commit('setJwtAccess', response.data.access)
      commit('setJwtRefresh', response.data.refresh)
    })
  },
  async refreshToken ({ commit }, token: string): Promise<boolean> {
    let success: boolean = false
    try {
      const response = await accountApi.refreshToken(token)
      commit('setJwtAccess', response.data.access)
      commit('setJwtRefresh', response.data.refresh)
      success = true
    } catch (e) {
      const status = e.response.status
      if (status === 400 || status === 401) {
        commit('removeJwtAccess')
        commit('removeJwtRefresh')
        success = false
      } else {
        throw e
      }
    }
    return success
  },
  async verifyToken ({ commit }, token: string): Promise<boolean> {
    let success: boolean = false
    try {
      await accountApi.verifyToken(token)
      success = true
    } catch (e) {
      const status = e.response.status
      if (status === 400 || status === 401) {
        commit('removeJwtAccess')
        commit('removeJwtRefresh')
        success = false
      } else {
        throw e
      }
    }
    return success
  },
  logout ({ commit }) {
    commit('removeJwtAccess')
    commit('removeJwtRefresh')
  }
}

const mutations: MutationTree<AuthState> = {
  setJwtAccess (state, token: string) {
    if (token !== '' && token != null) {
      state.jwtAccess = token
      Vue.cookies.set('access_token', token)
      Axios.defaults.headers.common.Authorization = 'Bearer ' + token
    }
  },
  removeJwtAccess (state) {
    state.jwtAccess = ''
    Vue.cookies.set('access_token', '')
    delete Axios.defaults.headers.common['Authorization']
  },
  setJwtRefresh (state, token: string) {
    if (token !== '' && token != null) {
      state.jwtRefresh = token
      Vue.cookies.set('refresh_token', token)
    }
  },
  removeJwtRefresh (state) {
    state.jwtRefresh = ''
    Vue.cookies.set('refresh_token', '')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
