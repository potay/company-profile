import * as MutationTypes from '@/store/webBluetooth/mutationTypes'
import { MutationTree } from 'vuex'
import { WebBluetoothState } from '@/store/webBluetooth'

const mutations: MutationTree<WebBluetoothState> = {
  [MutationTypes.BLE_SERVICE_ADDED] (state, service: BluetoothRemoteGATTService) {
    var serviceIndex = state.services.indexOf(service)
    if (serviceIndex < 0) {
      state.services.push(service)
    } else {
      state.services.splice(serviceIndex, 1, service)
    }
  },

  [MutationTypes.BLE_SERVICE_UPDATED] (state, service: BluetoothRemoteGATTService) {
    var serviceIndex = state.services.indexOf(service)
    if (serviceIndex < 0) return
    state.services.splice(serviceIndex, 1, service)
  },

  [MutationTypes.BLE_SERVICE_REMOVED] (state, service: BluetoothRemoteGATTService) {
    var serviceIndex = state.services.indexOf(service)
    if (serviceIndex < 0) return
    const characteristics = state.characteristics.filter(characteristic => service === characteristic.service)
    for (const characteristic of characteristics) {
      const characteristicIndex = state.characteristics.indexOf(characteristic)
      state.characteristics.splice(characteristicIndex, 1)
    }
    state.services.splice(serviceIndex, 1)
  }

}
export default mutations
