import * as MutationTypes from '@/store/webBluetooth/mutationTypes'
import { WebBluetoothState, BluetoothVuexDevice } from '@/store/webBluetooth'
import { MutationTree } from 'vuex'

const mutations: MutationTree<WebBluetoothState> = {
  [MutationTypes.BLE_DEVICE_UPDATED] (state, device: BluetoothVuexDevice) {
    state.device = device
  },
  [MutationTypes.BLE_DEVICE_REMOVED] (state) {
    state.device = null
    state.services = []
    state.characteristics = []
  }
}

export default mutations
