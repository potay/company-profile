import * as MutationTypes from '@/store/webBluetooth/mutationTypes'
import { WebBluetoothState } from '@/store/webBluetooth'
import { MutationTree } from 'vuex'

const mutations: MutationTree<WebBluetoothState> = {
  [MutationTypes.BLE_CHARACTERISTICS_DISCOVERED] (state, characteristics: BluetoothRemoteGATTCharacteristic[]) {
    characteristics.forEach(el => {
      var characteristicIndex = state.characteristics.findIndex(entry => {
        return entry === el
      })
      if (characteristicIndex < 0) {
        state.characteristics.push(el)
      } else {
        state.characteristics.splice(characteristicIndex, 1, el)
      }
    })
  },

  [MutationTypes.BLE_CHARACTERISTIC_CHANGED] (state, characteristic: BluetoothRemoteGATTCharacteristic) {
    var characteristicIndex = state.characteristics.findIndex(entry => {
      return entry === characteristic
    })
    if (characteristicIndex < 0) {
      state.characteristics.push(characteristic)
    } else {
      state.characteristics.splice(characteristicIndex, 1, characteristic)
    }
  }

}

export default mutations
