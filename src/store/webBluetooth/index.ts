import DeviceActions from './actions/device'
import ServiceActions from './actions/service'
import CharacteristicActions from './actions/characteristic'

import DeviceMutatations from './mutations/device'
import ServiceMutations from './mutations/service'
import CharacteristicMutations from './mutations/characteristic'
import { ActionTree, MutationTree, GetterTree } from 'vuex'
import { RootState } from '..'

export interface WebBluetoothState {
  device: BluetoothVuexDevice | null;
  services: BluetoothRemoteGATTService[];
  characteristics: BluetoothRemoteGATTCharacteristic[];
}

export interface BluetoothVuexDevice extends BluetoothDevice {
  gattDisconnectionCallback?: (event: any) => void;
  gattAdvertisementCallback?: (event: any) => void;
}

export interface BluetoothVuexDeviceOption {
  anyDevices?: boolean;
  name?: string;
  namePrefix?: string;
  services: string[];
}

const state: WebBluetoothState = {
  device: null,
  services: [],
  characteristics: []
}

const actions: ActionTree<WebBluetoothState, RootState> = Object.assign(
  {}, DeviceActions, ServiceActions, CharacteristicActions)

const mutations: MutationTree<WebBluetoothState> = Object.assign(
  {}, DeviceMutatations, ServiceMutations, CharacteristicMutations)

const getters: GetterTree<WebBluetoothState, RootState> = {
  device: (state: WebBluetoothState) => {
    return state.device
  },
  services: (state: WebBluetoothState) => {
    return state.services
  },
  service: (state: WebBluetoothState) => (serviceUuid: string) => {
    return state.services.find((service) => service.uuid === serviceUuid)
  },
  characteristics: (state: WebBluetoothState) => {
    return state.characteristics
  },
  characteristicsForService: (state: WebBluetoothState) => (service: BluetoothRemoteGATTService) => {
    return state.characteristics.filter((characteristic) => characteristic.service === service)
  },
  characteristicForService: (state: WebBluetoothState) => (service: BluetoothRemoteGATTService, characteristicUuid: string) => {
    return state.characteristics.find((characteristic) => characteristic.service === service && characteristic.uuid === characteristicUuid)
  }
}

export default {
  state: state,
  mutations: mutations,
  actions: actions,
  getters: getters,
  namespaced: true
}
