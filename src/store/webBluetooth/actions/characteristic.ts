import * as MutationTypes from '@/store/webBluetooth/mutationTypes'
import { ActionTree } from 'vuex'
import { WebBluetoothState } from '@/store/webBluetooth'
import { RootState } from '@/store'

interface DiscoverCharacteristicsQuery {
  service: BluetoothRemoteGATTService;
  characteristics: string[] | null;
}

const actions: ActionTree<WebBluetoothState, RootState> = {
  /**
    Query May consits of properties
    service mandatory
    characteristics optional
  **/
  async discoverCharacteristics ({ dispatch, commit }, query: DiscoverCharacteristicsQuery) {
    let discoveredCharacteristics: BluetoothRemoteGATTCharacteristic[] = []
    if (query.characteristics === undefined) {
      let characteristics = await query.service.getCharacteristics()
      for (let characteristic of characteristics) {
        await dispatch('configureCharacteristic', characteristic)
        discoveredCharacteristics.push(characteristic)
      }
    } else {
      for (let characteristicUUID of query.characteristics) {
        let characteristic = await query.service.getCharacteristic(characteristicUUID)
        if (characteristic) {
          await dispatch('configureCharacteristic', characteristic)
          discoveredCharacteristics.push(characteristic)
        }
      }
    }
    if (discoveredCharacteristics.length > 0) { commit(MutationTypes.BLE_CHARACTERISTICS_DISCOVERED, discoveredCharacteristics) }
  },
  async configureCharacteristic ({ dispatch, commit }, characteristicToConfigure: BluetoothRemoteGATTCharacteristic) {
    // Add an event listener that is triggered when the peripheral writes
    // a new value to this characteristic.
    if (
      characteristicToConfigure.properties.read ||
      characteristicToConfigure.properties.notify ||
      characteristicToConfigure.properties.indicate
    ) {
      characteristicToConfigure.addEventListener('characteristicvaluechanged', (_: any) => {
        dispatch('updateCharacteristic', characteristicToConfigure)
      })
    }
    // if allowed to subscribe to value change events, subscribe to the events
    // posted by the
    if (characteristicToConfigure.properties.notify || characteristicToConfigure.properties.indicate) {
      await characteristicToConfigure.startNotifications()
    }
  },
  async updateCharacteristic ({ commit }, characteristic: BluetoothRemoteGATTCharacteristic) {
    commit(MutationTypes.BLE_CHARACTERISTIC_CHANGED, characteristic)
  }
}

export default actions
