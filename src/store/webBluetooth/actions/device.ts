import * as mutationTypes from '@/store/webBluetooth/mutationTypes'
import { ActionTree } from 'vuex'
import { WebBluetoothState, BluetoothVuexDevice, BluetoothVuexDeviceOption } from '@/store/webBluetooth'
import { RootState } from '@/store'

const DeviceActions: ActionTree<WebBluetoothState, RootState> = {
  /*
    action launches the bluetooth native dialog to find devices in the proxmity
    the query can contain the following keys:
      * name
      * namePrefix
      * services - Setting services without a name or namePrefix will not return any results
      * optionalServices
  */
  async connectDevice ({ dispatch, commit }, query: BluetoothVuexDeviceOption) {
    var requestParameters: RequestDeviceOptions
    // Was a device name set for the character
    if (query.anyDevices) {
      requestParameters = {
        acceptAllDevices: true,
        optionalServices: []
      }
      requestParameters.optionalServices = query.services
    } else if (query.name || query.namePrefix) {
      requestParameters = {
        filters: [],
        optionalServices: []
      }
      if (query.name) {
        requestParameters.filters.push({ name: query.name })
      }
      if (query.namePrefix) {
        requestParameters.filters.push({ namePrefix: query.namePrefix })
      }
      requestParameters.optionalServices = query.services
    } else {
      requestParameters = {
        filters: [{ services: query.services }]
      }
    }

    // connect device
    let device: BluetoothVuexDevice = await navigator.bluetooth.requestDevice(requestParameters)
    if (device) {
      await device.gatt.connect()
      device.gattDisconnectionCallback = (_: any) => {
        device.removeEventListener('gattserverdisconnected', device.gattDisconnectionCallback)
        device.removeEventListener('advertisementreceived', device.gattAdvertisementCallback)
        commit(mutationTypes.BLE_DEVICE_REMOVED)
      }
      device.addEventListener('gattserverdisconnected', device.gattDisconnectionCallback)
      await dispatch('discoverServices', { device: device })
      commit(mutationTypes.BLE_DEVICE_UPDATED, device)
    }
    return device
  },

  async disconnectDevice ({ commit, state }) {
    let device: BluetoothVuexDevice = state.device
    if (device !== null && device.gatt.connected) {
      device.removeEventListener('gattserverdisconnected', device.gattDisconnectionCallback)
      device.removeEventListener('advertisementreceived', device.gattAdvertisementCallback)
      device.gatt.disconnect()
      commit(mutationTypes.BLE_DEVICE_UPDATED, device)
    }
    commit(mutationTypes.BLE_DEVICE_REMOVED)
  },

  async watchAdvertisements ({ commit, dispatch, state }) {
    let device: BluetoothVuexDevice = state.device
    if (device != null && device.gatt.connected) {
      // Add listener for RSSI
      device.gattAdvertisementCallback = (_: any) => {
        dispatch('webBluetoothDeviceAdvertisement', { advertisement: event })
        commit(mutationTypes.BLE_DEVICE_UPDATED, device)
      }
      device.addEventListener('advertisementreceived', device.gattAdvertisementCallback)
      await device.watchAdvertisements()
      commit(mutationTypes.BLE_DEVICE_UPDATED, device)
    }
  }
}

export default DeviceActions
