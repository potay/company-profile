import * as MutationTypes from '@/store/webBluetooth/mutationTypes'
import { ActionTree } from 'vuex'
import { WebBluetoothState, BluetoothVuexDevice } from '@/store/webBluetooth'
import { RootState } from '@/store'

interface DiscoverServicesQuery {
  device: BluetoothVuexDevice;
  uuid?: string;
}

const actions: ActionTree<WebBluetoothState, RootState> = {
  async discoverServices ({ dispatch, commit }, query: DiscoverServicesQuery) {
    if (query.uuid) {
      const service = await query.device.gatt.getPrimaryService(query.uuid)
      if (!service) return
      await dispatch('discoverCharacteristics', { service: service })
      commit(MutationTypes.BLE_SERVICE_ADDED, service)
    } else {
      const services = await query.device.gatt.getPrimaryServices()
      for (const service of services) {
        await dispatch('discoverCharacteristics', { service: service })
        commit(MutationTypes.BLE_SERVICE_ADDED, service)
      }
    }
  },

  async removeService ({ commit }, service: BluetoothRemoteGATTService) {
    if (service) {
      commit(MutationTypes.BLE_SERVICE_REMOVED, service)
    }
  }

}

export default actions
