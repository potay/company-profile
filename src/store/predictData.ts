import { ActionTree, GetterTree, MutationTree } from 'vuex'
import { RootState } from '@/store'

export interface PredictState {
  confident: boolean;
  texture: string;
  color: string;
  explanations: Array<string>;
  quizAnswers: Array<number>;
}

const state: PredictState = {
  confident: null,
  texture: null,
  color: null,
  explanations: null,
  quizAnswers: null
}

const getters: GetterTree<PredictState, RootState> = {
  confident: (state: PredictState) => {
    return state.confident
  },
  texture: (state: PredictState) => {
    return state.texture
  },
  color: (state: PredictState) => {
    return state.color
  },
  explanations: (state: PredictState) => {
    return state.explanations
  },
  quizAnswers: (state: PredictState) => {
    return state.quizAnswers
  }
}

const actions: ActionTree<PredictState, RootState> = {
  resetData ({ commit }) {
    commit('setConfident', null)
    commit('setTexture', null)
    commit('setExplanations', null)
    commit('setColor', null)
    commit('setQuizAnswers', null)
  }
}

const mutations: MutationTree<PredictState> = {
  setConfident (state, confident: boolean) {
    state.confident = confident
  },
  setTexture (state, texture: string) {
    state.texture = texture
  },
  setExplanations (state, explanations: Array<string>) {
    state.explanations = explanations
  },
  setColor (state, color: string) {
    state.color = color
  },
  setQuizAnswers (state, quizAnswers: Array<number>) {
    state.quizAnswers = quizAnswers
  },
  setPredictData (state, predictState: PredictState) {
    state.confident = predictState.confident
    state.texture = predictState.texture
    state.explanations = predictState.explanations
    state.color = predictState.color
    state.quizAnswers = predictState.quizAnswers
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
