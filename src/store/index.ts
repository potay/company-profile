import Vue from 'vue'
import Vuex from 'vuex'
import authentication from '@/store/authentication'
import webBluetooth from '@/store/webBluetooth'
import predictData from '@/store/predictData'

Vue.use(Vuex)

export interface RootState {}

export default new Vuex.Store({
  modules: {
    authentication,
    webBluetooth,
    predictData
  }
})
