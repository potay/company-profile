const PreloadWebpackPlugin = require('@vue/preload-webpack-plugin')

module.exports = {
  'configureWebpack': {
    'optimization': {
      'splitChunks': {
        'name': false,
        'chunks': 'all',
        'minSize': 512,
        'maxSize': 128 * 1024
      },
      'runtimeChunk': 'multiple',
      'mangleWasmImports': true
    }
  },
  'chainWebpack': config => config.plugin('VuetifyLoaderPlugin').tap(_ => [{}]),
  'outputDir': 'dist',
  'assetsDir': 'static',
  'productionSourceMap': false,
  'crossorigin': 'anonymous',
  'devServer': {
    'proxy': {
      '/api/*': {
        'target': 'http://localhost:8000/',
        'pathRewrite': {
          '^/api': ''
        }
      },
      '/sample/*': {
        'target': 'http://localhost:3000/',
        'pathRewrite': {
          '^/sample': ''
        }
      },
      '/predictor/*': {
        'target': 'http://localhost:9000/',
        'pathRewrite': {
          '^/predictor': ''
        }
      }
    }
  },
  'transpileDependencies': [
    'vuetify'
  ],
  'pluginOptions': {
    'webpackBundleAnalyzer': {
      'openAnalyzer': true
    }
  },
  'pwa': {
    'name': 'tot.bio',
    'themeColor': '#005b61',
    'msTileColor': '#005b61',
    'iconPaths': {
      'favicon32': 'img/icons/favicon-32x32.png',
      'favicon16': 'img/icons/favicon-16x16.png',
      'appleTouchIcon': 'img/icons/apple-touch-icon-152x152.png',
      'maskIcon': 'img/icons/safari-pinned-tab.svg',
      'msTileImage': 'img/icons/msapplication-icon-144x144.png'
    },
    'manifestPath': 'manifest.json',
    'manifestOptions': {
      'name': 'tot.bio Smart Toilet',
      'short_name': 'tot.bio',
      'description': 'Web Bluetooth based Smart Toilet by PoTay',
      'icons': [
        {
          'src': '/img/icons/favicon-16x16.png',
          'sizes': '16x16',
          'type': 'image/png'
        },
        {
          'src': '/img/icons/favicon-32x32.png',
          'sizes': '32x32',
          'type': 'image/png'
        },
        {
          'src': '/img/icons/android-chrome-192x192.png',
          'sizes': '192x192',
          'type': 'image/png'
        },
        {
          'src': '/img/icons/android-chrome-512x512.png',
          'sizes': '512x512',
          'type': 'image/png'
        }
      ],
      'start_url': '/',
      'display': 'standalone',
      'background_color': '#000000',
      'theme_color': '#005b61'
    },
    'workboxPluginMode': 'InjectManifest',
    'workboxOptions': {
      'swSrc': 'service-worker.js',
      'maximumFileSizeToCacheInBytes': 8 * 1024 * 1024,
      'globPatterns': [
        '**/*.{ico,png,jpg,jpeg,svg,html,xml,json,js,map,css,txt,eot,ttf,woff,woff2}'
      ]
    }
  }
}
